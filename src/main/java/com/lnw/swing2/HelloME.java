/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.swing2;

import java.awt.Color;
import java.awt.PopupMenu;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;

class MyActionListener implements ActionListener{

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Test My Action Listener : Action");
    }
    
}
/**
 *
 * @author Maintory_
 */
public class HelloME implements ActionListener {
    public static void main(String[] args) {
        JFrame frmMain = new JFrame();
        frmMain.setSize(400, 300);
        frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JLabel lblYourName = new JLabel("Yourname: ");
        lblYourName.setSize(80,20);
        lblYourName.setLocation(80,5);
        lblYourName.setBackground(Color.WHITE);
        lblYourName.setOpaque(true);
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(140,20);
        txtYourName.setLocation(170,5);
        
        
        JButton bthHello = new JButton("Hello");
        bthHello.setSize(80,20);
        bthHello.setLocation(140,40);
        
        MyActionListener myActionListener = new MyActionListener() {};
        bthHello.addActionListener(myActionListener);
        bthHello.addActionListener(new HelloME());
        
        ActionListener actionListener = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Annonymous Class:Action");
            }
        };
        bthHello.addActionListener(actionListener);
        
        JLabel lblHello = new JLabel("Hello...");
        lblHello.setSize(200,20);
        lblHello.setLocation(140,65);
        PopupMenu lblYourname;
        
        frmMain.setLayout(null);
        frmMain.add(lblYourName);
        frmMain.add(txtYourName);
        frmMain.add(bthHello);
        frmMain.add(lblHello);
       
        bthHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = txtYourName.getText();
                lblHello.setText("Hello " + name);
            }
        });
        
        frmMain.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloMeAction");
    }
}
